//require {defs, tiny} from './examples/common.js';
import {defs, tiny} from './examples/common.js';
//import {Obj_File_Demo} from "./examples/obj-file-demo.js"
import {Shape_From_File} from "./examples/obj-file-demo.js"
//Shape_From_File

const {
    Vector, Vector3, vec, vec3, vec4, color, hex_color, Shader, Matrix, Mat4, Light, Shape, Material, Scene, Texture,
} = tiny;

const {Cube, Axis_Arrows, Textured_Phong} = defs

export class Assignment4 extends Scene {
    /**
     *  **Base_scene** is a Scene that can be added to any display canvas.
     *  Setup the shapes, materials, camera, and lighting here.
     */
    constructor() {
        // constructor(): Scenes begin by populating initial values like the Shapes and Materials they'll need.
        super();

        // TODO:  Create two cubes, including one with the default texture coordinates (from 0 to 1), and one with the modified
        //        texture coordinates as required for cube #2.  You can either do this by modifying the cube code or by modifying
        //        a cube instance's texture_coords after it is already created.
        this.shapes = {
            box_1: new Cube(),
            box_2: new defs.Cube2(),
            axis: new Axis_Arrows(),
            complex_shape: new Cube(),
            "teapot": new Shape_From_File("assets/teapot.obj"),  // why the quotes?
            fish: new Shape_From_File("fish.obj"),
            herring: new Shape_From_File("herring.obj"),
        }
        console.log(this.shapes.box_1.arrays.texture_coord)

        // TODO:  Create the materials required to texture both cubes with the correct images and settings.
        //        Make each Material from the correct shader.  Phong_Shader will work initially, but when
        //        you get to requirements 6 and 7 you will need different ones.
        this.materials = {
            phong: new Material(new Textured_Phong(), {
                color: hex_color("#ffffff"),
            }),
            texture: new Material(new Textured_Phong(), {
                color: hex_color("#ffffff"),
                ambient: .7, diffusivity: 0.1, specularity: 0.1,
                texture: new Texture("assets/stars.png")
            }),
            scuba: new Material(new Textured_Phong(), {
                color: hex_color("#ffffff"),
                ambient: 0.4, diffusivity: 0.5, specularity: .1,
                texture: new Texture("assets/scuba.jpg")
            }),

            scubascroll: new Material(new Texture_Scroll_X(), {
                color: hex_color("#ffffff"),
                ambient: 0.4, diffusivity: 0.5, specularity: .1,
                texture: new Texture("assets/scuba.jpg", "NEAREST")
                //texture: new Texture("assets/scuba.jpg", min_filter="NEAREST")
            }),
            
            fives: new Material(new Texture_Rotate(), {
                color: hex_color("#ffffff"),
                ambient: 0.4, diffusivity: 0.5, specularity: .1,
                texture: new Texture("assets/fives.jpg", "LINEAR_MIPMAP_LINEAR")
                //texture: new Texture("assets/fives.jpg", min_filter="LINEAR_MIPMAP_LINEAR")

            }),   
                     
            xfives: new Material(new Textured_Phong(), {
                color: hex_color("#ffffff"),
                ambient: 0.4, diffusivity: 0.5, specularity: .1,
                texture: new Texture("assets/fives.jpg")
            }),
        }

        this.initial_camera_location = Mat4.look_at(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));

        this.old_t = 0;
        this.t  =    0;
        this.dt    = 0;   // incremental spin time only when actually spinning
    }

    
    zoom_back() {
        console.log ("Zoom Back");
        this.initial_camera_location = Mat4.look_at(vec3(0, 10, 100), vec3(0, 0, 0), vec3(0, 1, 0));
        //this.program_state.set_camera(this.initial_camera_location);
    }    
    zoom_top() {
        console.log ("Zoom Top");
        this.initial_camera_location = Mat4.look_at(vec3(0, 10, 0), vec3(0, 0, 0), vec3(1, 0, 0));
        //this.program_state.set_camera(this.initial_camera_location);
    }    
   zoom_front() {
        console.log ("Zoom Front");
        this.initial_camera_location = Mat4.look_at(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));
        //this.program_state.set_camera(this.initial_camera_location);
    }    
    zoom_start() {
        console.log ("Zoom Start");
        this.initial_camera_location = Mat4.look_at(vec3(0, 0,5), vec3(0, 0, 0), vec3(0, 1, 0));
        //program_state.set_camera(this.initial_camera_location);
    }
    zoom_angle() {
        console.log ("Zoom Angle");
        this.initial_camera_location = Mat4.look_at(vec3(5, 5, 5), vec3(0, 0, 0), vec3(0, 1, 0));
    }
    do_c () {
        this.spinning = !this.spinning;
        //console.log("^C has been pressed.");
        if(this.spinning)console.log("Spinning is on.")
        else console.log("Spinning is off.");
    }

    make_control_panel() {
        // TODO:  Implement requirement #5 using a key_triggered_button that responds to the 'c' key.

        this.key_triggered_button("Spin Cubes ", ["c"], this.do_c);
        this.new_line();
        this.key_triggered_button("Zoom Start ", ["Control", "0"], this.zoom_start);
        this.new_line();
        this.key_triggered_button("Zoom top ",   ["Control", "1"], this.zoom_top);
        this.new_line();
        this.key_triggered_button("Zoom Front ", ["Control", "2"], this.zoom_front);
        this.new_line();
        this.key_triggered_button("Zoom Angle ", ["Control", "3"], this.zoom_angle);
    }

    display(context, program_state) {

        // handle time
        let t = program_state.animation_time / 400;
        let dt = program_state.animation_delta_time / 400;
        let d=0;
        if(this.t==0)this.t = t;
        this.old_t = this.t;
        this.t = t;
        //this.dt = 0;
        if(this.spinning) {
             d = this.t - this.old_t;
            this.dt = this.dt + d;
            //console.log("Spinning dt = " + this.dt);
        }


        // Set up the camera

        if (!context.scratchpad.controls) {
            this.children.push(context.scratchpad.controls = new defs.Movement_Controls());
            // Define the global camera and projection matrices, which are stored in program_state.
            program_state.set_camera(Mat4.translation(0, 0, -10));
        }
        program_state.set_camera(this.initial_camera_location);   // set camera position per customer 
        
        program_state.projection_transform = Mat4.perspective(
            Math.PI / 4, context.width / context.height, 1, 100);

        const light_position = vec4(10, 10, 10, 1);
        program_state.lights = [new Light(light_position, color(1, 1, 1, 1), 1000)];


        console.log("Draw staircase");
        var i=0;
        var h=0;
        var theta = 0;
        var r = .4;
        for(i=0; i<=20; i++) {
            console.log("draw step herring " + i);
  //          h=i/5;
            h=i;  // more spacing
            let m = Mat4.identity();

            m=m.times(Mat4.translation(0.,h,-5));
            theta = i/2;
            m=m.times(Mat4.rotation(theta, 0, 1, 0));
            m=m.times(Mat4.translation(1,0.,0.));
            // m=m.times(Mat4.scale(1, .15, .4));
            // this.shapes.box_1.draw(context, program_state, m, this.materials.phong);
            // this.shapes.fish.draw(context, program_state, m, this.materials.phong);
  
            // herring sideways, flip it right side up
            //             rotate(theta, normal)
            m=m.times(Mat4.rotation(3.14159/2, 1, 0, 0));

          this.shapes.herring.draw(context, program_state, m, this.materials.phong);
        }


        //console.log("Draw a fish");

        

      
        //this.shapes = {"teapot": new Shape_From_File("assets/teapot.obj")};

        // Draw box 1

        let model_transform = Mat4.identity();
        model_transform = model_transform.times(Mat4.translation(-2.,0.,0.));
        model_transform = model_transform.times(Mat4.rotation(1.5*this.dt,1.,0.,0.));
        
//        this.shapes.box_1.draw(context, program_state, model_transform,  this.materials.fives);

        // Draw box 2

        let model_transform2 = Mat4.identity();         
        model_transform2 = model_transform2.times(Mat4.translation(2.,0.,0.));
        model_transform2 = model_transform2.times(Mat4.rotation(1.0*this.dt,0.,1.,0.));
        //this.shapes.box_2.draw(context, program_state, model_transform2, this.materials.scubascroll);
        

        /*  // reminder of scale and rotation commands
                this.moon = this.planet_4.times(Mat4.rotation(theta_moon,0,1,0))  // rotate direction to moon
           .times(Mat4.translation(r_moon,0,0,))                          // offset moon
           .times(Mat4.rotation(moon_dtheta,0,1,0));                      // rotate the moon

        //this.moon = this.planet_4.times(Mat4.translation(x_moon, 0, z_moon));  // set the location
        //this.moon = this.moon.times(Mat4.rotation(theta_moon, 0, 1, 0));  // rotate around y axis
        
        this.shapes.sphere4.draw(context, program_state, this.moon.times(Mat4.scale(moon_radius, moon_radius, moon_radius)), 
           this.materials.test.override({color: mist}));
           */
    }
}


class Texture_Scroll_X extends Textured_Phong {
    // TODO:  Modify the shader below (right now it's just the same fragment shader as Textured_Phong) for requirement #6.
    fragment_glsl_code() {
        return this.shared_glsl_code() + `
            varying vec2 f_tex_coord;
            uniform sampler2D texture;
            uniform float animation_time;
            
            void main(){
                // Sample the texture image in the correct place:

                vec2 vv = f_tex_coord.xy;   // copy picture coordinate into variable
                //vv.x = vv.x + .5;   // move it over
                vv.y = vv.y + mod(animation_time,2.);
                
                vec4 tex_color = texture2D( texture, vv.xy);
                // vec4 tex_color = texture2D( texture, f_tex_coord);

                // to make picture scroll, sample a different location

                if( tex_color.w < .01 ) discard;

                gl_FragColor = tex_color;

               // The next part washes out the image
                                                                         // Compute an initial (ambient) color:
               // gl_FragColor = vec4( ( tex_color.xyz + shape_color.xyz ) * ambient, shape_color.w * tex_color.w ); 
                                                                         // Compute the final color with contributions from lights:
              //  gl_FragColor.xyz += phong_model_lights( normalize( N ), vertex_worldspace );
        } `;
    }
}


class Texture_Rotate extends Textured_Phong {
    // TODO:  Modify the shader below (right now it's just the same fragment shader as Textured_Phong) for requirement #7.
    fragment_glsl_code() {
        return this.shared_glsl_code() + `
            varying vec2 f_tex_coord;
            uniform sampler2D texture;
            uniform float animation_time;
            void main(){
                // Sample the texture image in the correct place:
                vec4 tex_color = texture2D( texture, f_tex_coord );

                vec2 vv = f_tex_coord.xy;   // copy picture coordinate into variable

                vv.x -=.5;
                vv.y-=.5;

                float r = sqrt( vv.x * vv.x + vv.y*vv.y);
                vec2 nuv;
                float theta;
                if(vv.x==0.)theta = 3.14159/2.;
                else theta=atan(vv.y/vv.x);

                if(vv.x < 0.) theta = theta + 3.14159265;

                theta = theta + mod(animation_time,99.);

                nuv.x = r * cos(theta)+.5;
                nuv.y = r * sin(theta)+.5;






                vec4 tex_colorx = texture2D( texture, nuv.xy);
                gl_FragColor = tex_colorx;
          //      gl_FragColor.xyz *= phong_model_lights( normalize( N ), vertex_worldspace );


        //        return;

        //        if( tex_color.w < .01 ) discardr.xyz + shape_color.xyz ) * ambient, shape_color.w * tex_color.w ); 
                                               ;
                                                                         // Compute an initial (ambient) color:
        //        gl_FragColor = vec4( ( tex_colo                          // Compute the final color with contributions from lights:
        //        gl_FragColor.xyz += phong_model_lights( normalize( N ), vertex_worldspace );
        } `;
    }
}

