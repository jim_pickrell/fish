//<script>

//============================ PVector ===========================

// static functions

function pv_sub(a,b) { // subtract vectors and assign to new object
   let c = new PVector(a.x, a.y, a.z);
   c.x = a.x - b.x;
   c.y = a.y - b.y;
   c.z = a.z - b.z;
   return c;
}

function pv_add(a,b) { // add vectors and assign to new object
   let c = new PVector(a.x, a.y, a.z);
   c.add(b);
   return c;
}

function pv_dist(a,b) { // distance between points
   let x = a.dist(b);
   return x;
}

// Define the PVector object

class PVector {
   constructor(x, y, z) {
      this.x = x;
      this.y = y;
      this.z = z;
   }

   zadd(other) {  // add points
   let p = new PVector(0,0,0);
      p.x = this.x + other.x;
      p.y = this.y + other.y;
      p.z = this.z + other.z;
      return p;
   }
   add(other) {  // add points
   let p = new PVector(0,0,0);
      this.x = this.x + other.x;
      this.y = this.y + other.y;
      this.z = this.z + other.z;
      return this;
   }

   sub(other) {  // add points
      this.x = this.x - other.x;
      this.y = this.y - other.y;
      this.z = this.z - other.z;
      return this;
   }

   div(z) {  // divide point
      this.x = this.x / z;
      this.y = this.y / z;
      this.z = this.z / z;
      return this;
   }

   mult(z) { // scale point
      this.x = this.x * z;
      this.y = this.y * z;
      this.z = this.z * z;
      return this;
   }

   mag() { // magnitude (length)
     var r = this.x * this.x + this.y * this.y + this.z + this.z;
     r = Math.sqrt(r);
     return r;
   }

   normalize() { // normalize to length = 1
     var r = this.x * this.x + this.y * this.y + this.z * this.z;
     r = Math.sqrt(r);
     this.x = this.x / r;
     this.y = this.y / r;
     this.z = this.z / r;
     return this; 
   }

   limit(v) { // limit length/magnitude
      var r = this.x * this.x + this.y * this.y + this.z * this.z;
      r = Math.sqrt(r);
      if(r <= v) {
        return; 
      }
      var s = v/r;
      this.x *= s;
      this.y *= s;
      this.z *= s;
   }

   dist(other) { // distance between points
      var u = (this.x - other.x) * (this.x - other.x);
      var v = (this.y - other.y) * (this.y - other.y);
      var w = (this.z - other.z) * (this.z - other.z);
      var d = Math.sqrt(u+v+w);
      return d; 
   }

   angle() { // find angle to points
      var PI = 3.14159;
      var a=0;
      if(this.x == 0) {
         a=PI/2;   
      }
      else {
         a=Math.atan(this.y / this.x);
      }
      if(this.x <0) {
        a += PI;
      }
      return a;
   }

   anglez () { // find up angle to point
      var PI = 3.14159;
      var a=0;
      if(this.x == 0  && this.z == 0) {
         a=PI/2;   
      }
      else {
         a=Math.atan(this.z / Math.sqrt(this.x*this.x + this.y*this.y));
      }
//      if(this.x <0) {  
//         a += PI;  // radius is always positive, and z angle is -PI/2 < az < PI/2
//      }
      return a;
   }

}

// tests for vector math routines


export function do_math_test() {


   console.log("Math test");

   let a = new PVector(2,2,2);
   let b = new PVector(3,0,0);
   let c = new PVector(4,0,-.3);
   let d = new PVector(0,5,0);
   let e = new PVector(-6,-6,-6);

   let f = new PVector(0,0,0);

   //functions

   f = pv_add(a,c);

   console.log("Start values:")
   console.log("a=" + JSON.stringify(a));
   console.log("b=" + JSON.stringify(b));
   console.log("c=" + JSON.stringify(c));
   console.log("d=" + JSON.stringify(d));
   console.log("e=" + JSON.stringify(e));

   console.log("Testing static fuctions");

   console.log("f=pv_add(a,b)");
   
   console.log("a+a=" + JSON.stringify(pv_add(a,a)));
   console.log("a+b=" + JSON.stringify(pv_add(a,b)));
   console.log("d+e=" + JSON.stringify(pv_add(d,e)));
   console.log("e+e=" + JSON.stringify(pv_add(e,e)));
   console.log("a+c=" + JSON.stringify(pv_add(a,c)));

   console.log("f=pv_sub(a,b)");
   
   console.log("a-a=" + JSON.stringify(pv_sub(a,a)));
   console.log("a-b=" + JSON.stringify(pv_sub(a,b)));
   console.log("d-e=" + JSON.stringify(pv_sub(d,e)));
   console.log("e-e=" + JSON.stringify(pv_sub(e,e)));
   console.log("a-c=" + JSON.stringify(pv_sub(a,c)));

   console.log("f=pv_dist(a,b)");
   
   console.log("dist(a,a)=" + JSON.stringify(pv_dist(a,a)));
   console.log("dist(a,b)=" + JSON.stringify(pv_dist(a,b)));
   console.log("dist(a,e)=" + JSON.stringify(pv_dist(d,e)));
   console.log("dist(e,e)=" + JSON.stringify(pv_dist(e,e)));
   console.log("dist(a,c)=" + JSON.stringify(pv_dist(a,c)));

   console.log("Test static functions.");
   console.log("a.add(a)" + JSON.stringify(a.add(a)));
   console.log("a.add(b)" + JSON.stringify(a.add(b)));
   console.log("a.add(c)" + JSON.stringify(a.add(c)));
   console.log("a.add(d)" + JSON.stringify(a.add(d)));
   console.log("a.add(e)" + JSON.stringify(a.add(e)));

   console.log("a.sub(a)" + JSON.stringify(a.sub(a)));
   console.log("a.sub(b)" + JSON.stringify(a.sub(b)));
   console.log("a.sub(c)" + JSON.stringify(a.sub(c)));
   console.log("a.sub(d)" + JSON.stringify(a.sub(d)));
   console.log("a.sub(e)" + JSON.stringify(a.sub(e)));

   let x=3;
   a=new PVector(2,2,2);
   console.log("x=3");
   console.log("a.mult(x)=" + JSON.stringify(a.mult(x)));
   console.log("b.mult(x)=" + JSON.stringify(b.mult(x)));
   console.log("c.mult(x)=" + JSON.stringify(c.mult(x)));
   console.log("d.mult(x)=" + JSON.stringify(d.mult(x)));
   console.log("e.mult(x)=" + JSON.stringify(e.mult(x)));

   console.log("a.div(x)=" + JSON.stringify(a.div(x)));
   console.log("b.div(x)=" + JSON.stringify(b.div(x)));
   console.log("c.div(x)=" + JSON.stringify(c.div(x)));
   console.log("d.div(x)=" + JSON.stringify(d.div(x)));
   console.log("e.div(x)=" + JSON.stringify(e.div(x)));

   console.log("a.mag()=" + JSON.stringify(a.mag()));
   console.log("b.mag()=" + JSON.stringify(b.mag()));
   console.log("c.mag()=" + JSON.stringify(c.mag()));
   console.log("d.mag()=" + JSON.stringify(d.mag()));
   console.log("e.mag()=" + JSON.stringify(e.mag()));

   console.log("a.normalize()=" + JSON.stringify(a.normalize()));
   console.log("b.normalize()=" + JSON.stringify(b.normalize()));
   console.log("c.normalize()=" + JSON.stringify(c.normalize()));
   console.log("d.normalize()=" + JSON.stringify(d.normalize()));
   console.log("e.normalize()=" + JSON.stringify(e.normalize()));






}



//================================== FISH =========================

// The fish class

var fish_count = 0;
export class Fish {  // these are our fish
   //var position;
   //var velocity, acceleration;
   //var r, maxforce, maxspeed;

   constructor(x, y, z) {  // create a fish
      // Fish generally are started at the middle 
      // All start with the same velocity but in different directions. 
      console.log("Creating fish: " + x + ", " + y + ", " + z);
      let PI = 3.14159;
      this.name = "Fish_" + fish_count;
      this.acceleration = new PVector(0,0,0);
      this.angle = Math.random() * 2 * PI;
      this.anglez = -PI/2 + (Math.random() * PI);  // about -1.6 to 1.6
      this.velocity = new PVector(Math.cos(this.angle), Math.sin(this.angle), Math.sin(this.anglez));  // z direction is haphazard but should be ok
      this.position = new PVector(x,y,z);
      this.r=2.0;
      this.maxspeed = 2;
      this.maxforce = 0.03;
      fish_count++;
   }

   description() {
       var theta = this.velocity.angle();
       var phi = this.velocity.anglez();
       var s = this.name + " " + this.position.x + ", " + this.position.y + ", " + this.position.z + " angle=" + theta + " anglez=" + phi;
       return s;
   }

   move(fish_list) { // move for this fish depends on the whole list
      this.flock(fish_list);
      this.update();
      this.borders();
      this.render();
   }

   applyForce(force) { // modify acceleration for this fish
       this.acceleration.add(force);
   }

   // We accumulate a new acceleration each time based on three rules
   flock(fish_list) {
      let sep = this.separate(fish_list);   // Separation
      let ali = this.align(fish_list);      // Alignment
      let coh = this.cohesion(fish_list);   // Cohesion
    
      // Arbitrarily weight these forces
      sep.mult(1.5);
      ali.mult(1.0);  
      coh.mult(1.0);
    
      // Add the force vectors to acceleration
      this.applyForce(sep);  
      this.applyForce(ali);
      this.applyForce(coh);
   }

   // Method to update position, acceleration and velocity

   update() {
      // Update velocity
      this.velocity.add(this.acceleration);  
      // Limit speed
      this.velocity.limit(this.maxspeed);
      this.position.add(this.velocity);
      // Reset accelertion to 0 each cycle
      this.acceleration.mult(0);
   }

   // A method that calculates and applies a steering force towards a target
   // STEER = DESIRED MINUS VELOCITY
   seek(target) {
      var desired = pv_sub(target, this.position);  // A vector pointing from the position to the target
      // Scale to maximum speed
      desired.normalize();
      desired.mult(this.maxspeed);

      // Steering = Desired minus Velocity
      var steer = pv_sub(desired, this.velocity);
      steer.limit(this.maxforce);  // Limit to maximum steering force
      return steer;
  }

  render() {
/*
    // Draw a triangle rotated in the direction of velocity
    float theta = velocity.heading2D() + radians(90);
    // heading2D() above is now heading() but leaving old syntax until Processing.js catches up
    
    fill(200, 100);
    stroke(255);
    pushMatrix();
    translate(position.x, position.y);
    rotate(theta);
    beginShape(TRIANGLES);
    vertex(0, -r*2);
    vertex(-r, r*2);
    vertex(r, r*2);
    endShape();
    popMatrix();
*/
  let theta = this.velocity.angle();
  let phi = this.velocity.anglez();  ////>>>>>???????
  console.log("Rendering object: " + this.position.x + ", " + this.position.y + ", " + this.position.z + " a=" + theta + " az=" + phi); 
  }

  // Wraparound - keep the fish in our test area 
  borders() {
    var r = this.r;  //2    size of the fish  
     if (this.position.x < -r) this.position.x = game_width+r;
     if (this.position.y < -r) this.position.y = game_height+r;
     if (this.position.z < -r) this.position.z = game_depth+r;
     if (this.position.x > game_width+r)  this.position.x = -r;
     if (this.position.y > game_height+r) this.position.y = -r;
     if (this.position.z > game_depth+r)  this.position.z = -r;
  }

  // Separation
  // Method checks for nearby fish and steers away

  separate (fishes) {
     var desiredseparation = 25.0;
     var steer = new PVector(0, 0, 0);
     var count = 0;

     // For every fish in the system, check if it's too close



     // I think no fix is required for 3d



     for (let other of fishes) {
        var d = pv_dist(this.position, other.position);
        // If the distance is greater than 0 and less than an 
        // arbitrary amount (0 when you are yourself)

        if ((d > 0) && (d < desiredseparation)) {
           // Calculate vector pointing away from neighbor
           var diff = pv_sub(this.position, other.position);
           diff.normalize();
           diff.div(d);        // Weight by distance
           steer.add(diff);
           count++;            // Keep track of how many
        }
     }
     // Average -- divide by how many
     if (count > 0) {
        steer.div(count);
     }

     // As long as the vector is greater than 0
     if (steer.mag() > 0) {

        // Implement Reynolds: Steering = Desired - Velocity
        steer.normalize();
        steer.mult(this.maxspeed);
        steer.sub(this.velocity);
        steer.limit(this.maxforce);
     }
     return steer;
   }
   
   // Alignment

   // For every nearby boid in the system, calculate the average velocity
   align (fish_list) {  
      var neighbordist = 50;
      var sum = new PVector(0, 0, 0);
      var count = 0;
      for (let other of fish_list) {
         var d = pv_dist(this.position, other.position);
         if ((d > 0) && (d < neighbordist)) {
            sum.add(other.velocity);
            count++;
         }
       }
    
      if (count > 0) {
         sum.div(count);

         // Implement Reynolds: Steering = Desired - Velocity

         sum.normalize();
         sum.mult(this.maxspeed);
         var steer = pv_sub(sum, this.velocity);
         steer.limit(this.maxforce);
         return steer;
      } 
      else {
         return new PVector(0, 0, 0);
      }
   }

   // Cohesion method

   // For the average position (i.e. center) of all nearby fish, calculate steering vector towards that position
   cohesion (fish_list) {
      var neighbordist = 50;
      var sum = new PVector(0, 0, 0);   // Start with empty vector to accumulate all positions
      var count = 0;

      for (let other of fish_list) {
         var d = this.position.dist(other.position);
         if ((d > 0) && (d < neighbordist)) {
            sum.add(other.position); // Add position
            count++;
         }
       }
       if (count > 0) {
          sum.div(count);
          return this.seek(sum);  // Steer towards the position
       } 
       else {
          return new PVector(0, 0, 0);
       }
   }
}

//=================================== SCHOOL ===================================

export class School {
   constructor() {
      console.log("Creating a School");
      this.fish_list=[];
   }

   run() {

      for(let f of this.fish_list) { 
          f.move(this.fish_list);
      }
   }

   add_fish(b) {
      this.fish_list.push(b);
   }

   display(i) {
     
      let vx=0;
      let vy=0;
      let vz=0;
      let fishcount = 0;

      var my_text = "Turn " + i + "<br /><pre>";
      for(let f of this.fish_list) {
         my_text += f.description();
         vx += f.velocity.x;
         vy += f.velocity.y;
         vz += f.velocity.z;
         fishcount++;
         my_text += "<br />";
         fishcount++;

      }
      vx = vx / fishcount;
      vy = vy / fishcount;
      vz = vz / fishcount;

      my_text += "Avg V = " + vx + ", " + vy + ", " + vz ;
      my_text +="</pre>";
      document.getElementById("line3").innerHTML = my_text;
   }
}

