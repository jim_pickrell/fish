// Jim Pickrell
// UCLA CS 275
// Final Project

// c 2021
import {defs, tiny} from './examples/common.js';
import {Shape_From_File} from "./examples/obj-file-demo.js"
import {Fish, School, Game, PVector, pv_dist} from './fish_engine.js'
import {Food} from './food_engine.js'

const {
    Vector, Vector3, vec, vec3, vec4, color, hex_color, Shader, Matrix, Mat4, Light, Shape, Material, Scene, Texture,
} = tiny;

const {Cube, Axis_Arrows, Textured_Phong} = defs

export class Fish_scene extends Scene {
   /**
    *  **Base_scene** is a Scene that can be added to any display canvas.
    *  Setup the shapes, materials, camera, and lighting here.
    */
   constructor() {
      // constructor(): Scenes begin by populating initial values like the Shapes and Materials they'll need.
      super();

      this.game = new Game();

      // All OBJ files should be loaded here.
      this.shapes = {
         cube: new Cube(),
         cylinder: new Shape_From_File("models/cylinder.obj"),
         ball: new Shape_From_File("models/sphere6.obj"),
         //box_2: new defs.Cube2(),
         //axis: new Axis_Arrows(),
         //complex_shape: new Cube(),
         //"teapot": new Shape_From_File("assets/teapot.obj"),  // why the quotes?
         fish: new Shape_From_File("models/fish.obj"),
         fish2: new Shape_From_File("models/fish2.obj"),
         herring: new Shape_From_File("models/herring.obj"),
         simple_fish: new Shape_From_File("models/simple_fish.obj"),
         shark: new Shape_From_File("models/shark_CM.obj"),
         shark_OM: new Shape_From_File("models/shark_OM.obj"),

         // animated shark sequence
         shark_2l: new Shape_From_File("models/shark_2l.obj"),
         shark_1l: new Shape_From_File("models/shark_1l.obj"),
         shark_c: new Shape_From_File("models/shark_c.obj"),
         shark_1r: new Shape_From_File("models/shark_1r.obj"),
         shark_2r: new Shape_From_File("models/shark_2r.obj"),
         // Shrimp
         food: new Shape_From_File("models/copepod.obj"),

      }
      //console.log(this.shapes.box_1.arrays.texture_coord)
// debugger;

      this.materials = {
         red_phong: new Material(new defs.Phong_Shader(), {
               color: hex_color("#ec3aaa"),
               ambient: .5, diffusivity: 0.3, specularity: 0.3,
         }),
         phong: new Material(new defs.Phong_Shader(), {
               color: hex_color("#ffffff"),
               ambient: .3, diffusivity: 0.3, specularity: 0.3,
         }),
         texture: new Material(new Textured_Phong(), {
               color: hex_color("#ffffff"),
               ambient: .7, diffusivity: 0.1, specularity: 0.1,
               texture: new Texture("assets/stars.png")
         }),
      }

      // Set the initial camera position here.
      // x,y,z with x right, left up, z coming at you
      // Vectors are camera, target, a vector showing what is up
      // this.initial_camera_location = Mat4.look_at(vec3(50, -50, -40), vec3(0, 0, 0), vec3(0, 1, 0));

      // from pinball
      // this.initial_camera_location = Mat4.look_at(vec3(35, -55, 90), vec3(35, 45, 0), vec3(0, 1, 1));
      


      // This is the start location of the camera
      //
      this.initial_camera_location = Mat4.translation(0, 0, -150);

      this.old_t = 0;
      this.t  =    0;
      this.dt    = 0;   // incremental spin time only when actually spinning

      // Variables for food
      this.food_list = [];
      this.jump_interval = 3;
   }
    
   zoom_back() {
      console.log ("Zoom Back");
      this.initial_camera_location = Mat4.look_at(vec3(0, 0, 100), vec3(0, 0, 0), vec3(0, 1, 0));
      //this.program_state.set_camera(this.initial_camera_location);
   }    
   zoom_top() {
      console.log ("Zoom Top");
      this.initial_camera_location = Mat4.look_at(vec3(0, 100, 0), vec3(0, 0, 0), vec3(1, 0, 0));
      //this.program_state.set_camera(this.initial_camera_location);
   }    
   zoom_front() {
      console.log ("Zoom Front");
      this.initial_camera_location = Mat4.look_at(vec3(0, 0, -100), vec3(0, 0, 0), vec3(0, 1, 0));
      //this.program_state.set_camera(this.initial_camera_location);
   }    
   zoom_start() {
      console.log ("Zoom Start");
      this.initial_camera_location = Mat4.look_at(vec3(0, 0,-60), vec3(0, 0, 0), vec3(0, 1, 0));
      //program_state.set_camera(this.initial_camera_location);
   }
   zoom_angle() {
      console.log ("Zoom Angle");
      this.initial_camera_location = Mat4.look_at(vec3(100, 100, -100), vec3(0, 0, 0), vec3(0, 1, 0));
   }

   add_food() {
       // debugger;
       let food_coord = new PVector(Math.floor(Math.random() * 40)-20, Math.floor(Math.random() * 30)-15, Math.floor(Math.random() * 30)-15);
       while(this.on_obstacles(food_coord)) {
           console.log("Keep generating");
           food_coord = new PVector(Math.floor(Math.random() * 40)-20, Math.floor(Math.random() * 30)-15, Math.floor(Math.random() * 30)-15);
       }
       // debugger; 
       let f = new Food(food_coord.x, food_coord.y, food_coord.z);
       this.food_list.push(f);
       //this.food_list.push(new Food(food_coord.x, food_coord.y, food_coord.z));
       console.log ("Add Food: " + JSON.stringify(f));

   }

   on_obstacles(food_coord) {
       let smallest_dist = Number.MAX_SAFE_INTEGER;
       let max_vision_range = 25;
       //console.log("Caculate distance");
       for (let obstacle of this.game.school.obstacle_list) {
           let dist = pv_dist(food_coord, obstacle.position);
           if(dist < smallest_dist) {
               smallest_dist = dist;
           }
       }
       if(smallest_dist < max_vision_range) {
           return true;
       }
       return false;
   }

//   do_c () {
//      this.spinning = !this.spinning;
//      //console.log("^C has been pressed.");
//      if(this.spinning)console.log("Spinning is on.")
//      else console.log("Spinning is off.");
//   }

   make_control_panel() {
      // this.key_triggered_button("Spin Cubes ", ["c"], this.do_c);
      // this.new_line();
      // debugger;
      this.key_triggered_button("Zoom Start ", ["Control", "0"], this.zoom_start);
      this.new_line();
      this.key_triggered_button("Zoom top ",   ["Control", "1"], this.zoom_top);
      this.new_line();
      this.key_triggered_button("Zoom Front ", ["Control", "2"], this.zoom_front);
      this.new_line();
      this.key_triggered_button("Add Food ", ["Control", "3"], this.add_food);

      //this.new_line();
      //this.key_triggered_button("Zoom Angle ", ["Control", "3"], this.zoom_angle);
   }

   draw_obstacle(context, program_state, f) { 

      let x=f.position.x;
      let y=f.position.y;
      let z=f.position.z;
      let r=f.r;
      let h=f.h;

      //console.log("draw_fish " + i);

      let m = Mat4.identity();      
 
      m = m.times(Mat4.translation(x, y, -z));     // translate the rotated fish into position

      // this area can be optimized to omit calculations for dead fish.

      let my_scale = 1;

      if(f.type == "obstacle") {
          m=m.times(Mat4.scale(r, r, r));
          this.shapes.cube.draw(context, program_state, m, this.materials.phong);
      }

      if(f.type == "ball") {
          m=m.times(Mat4.scale(r, r, r));
          this.shapes.ball.draw(context, program_state, m, this.materials.phong);
      }

      if(f.type == "cylinder") {
           m=m.times(Mat4.scale(r, h, r));
           this.shapes.cylinder.draw(context, program_state, m, this.materials.phong);        
      }
      return;
   }

   draw_fish(context, program_state, f) { 

      let x=f.position.x;
      let y=f.position.y;
      let z=f.position.z;
      let a=f.theta();
      let az=f.phi();

      //console.log("draw_fish " + i);

      let m = Mat4.identity();      

      // Let's try offsetting the shark a little bit to move center point
      if(f.type == "no_shark") {  // disabled
             m = m.times(Mat4.translation(-.2,0,0));
      }

      m = m.times(Mat4.translation(x, y, -z));     // translate the rotated fish into position
      m = m.times(Mat4.rotation(a, 0, 1, 0));     // rotate fish in horizontal plane (around y)
      m = m.times(Mat4.rotation(az, 0, 0, 1));    // rotate up (around z)
    
      //m = m.times(Mat4.rotation(az, 0, 0, 1));    // rotate up (around z)
      //m = m.times(Mat4.rotation(a, 0, 1, 0));     // rotate fish in horizontal plane (around y)
      //m = m.times(Mat4.translation(x, y, z));     // translate the rotated fish into position
      
      //m

      // this area can be optimized to omit calculations for dead fish.

      let my_scale = 2;

      if(f.type == "fish"  && f.live == true) {
          my_scale = 2;
          m=m.times(Mat4.scale(my_scale, my_scale, my_scale));
          this.shapes.fish2.draw(context, program_state, m, this.materials.phong);
      }

      if(f.type == "shark") {
           my_scale = 5;
           m=m.times(Mat4.scale(my_scale, my_scale, my_scale));


           // if we knew if the shark is turning left or right we could
           // use the images shark_2l, shark_1l, shark_c, shark_1r, shark_2r
           // to bend the shark appropriately
           console.log(f.mouth_state);
           if(f.mouth_state == "shark_CM"){
              this.shapes.shark.draw(context, program_state, m, this.materials.phong);        
           }
           else if(f.mouth_state == "shark_OM"){
              this.shapes.shark_OM.draw(context, program_state, m, this.materials.phong);        
           }
           
      }
      return;
   }


    draw_food(context, program_state, t) {
        console.log("Draw food called.");
        for(let i = 0;i < this.food_list.length;i++) {
            if(this.food_list[i].eaten == false) {
                console.log(this.food_list[i].jump == true);
                if(this.food_list[i].jump == true && this.food_list[i].lastJumpTime + this.jump_interval < t) {
                    this.food_list[i].lastJumpTime = t;
                    this.food_list[i].jump = false;
                    this.food_list[i].position.y = this.food_list[i].position.y + 8;
                    let position = this.food_list[i].get_position();
                    let location = Mat4.identity().times(Mat4.translation(position.x, position.y, position.z));
                    this.shapes.food.draw(context, program_state, location, this.materials.red_phong);
                } else {
                    let position = this.food_list[i].get_position();
                    let location = Mat4.identity().times(Mat4.translation(position.x, position.y, position.z));
                    this.shapes.food.draw(context, program_state, location, this.materials.red_phong);
                }

            }
        }
    }



   display(context, program_state) {

      // This is the master drawing loop.
      // It is called from Tiny Graphics 
      // There is no paticular frame rate, it just runs as fast as it can
      // If you want to know how much time has elapsed since the last frame
      // you have to do a little calculation

      // The following code is an example of how to do that.  It is not used by the fish simulation.
      //
      //  --Jim
      //
      // Handle time
      let t = program_state.animation_time / 400;
      let dt = program_state.animation_delta_time / 400;
      let d=0;
      if(this.t==0)this.t = t;
      this.old_t = this.t;
      this.t = t;
      //this.dt = 0;
      if(this.spinning) {
         d = this.t - this.old_t;
         this.dt = this.dt + d;
         //console.log("Spinning dt = " + this.dt);
      }

      // Set up the camera

      // In tiny graphics the camera is defined by three points
      // 1. Camera position
      // 2. Target position
      // 3. A third point indicating the Up direction

      if (!context.scratchpad.controls) {
         this.children.push(context.scratchpad.controls = new defs.Movement_Controls());
         // Define the global camera and projection matrices, which are stored in program_state.
         program_state.set_camera(Mat4.translation(0, 0, -20));
      }
      program_state.set_camera(this.initial_camera_location);   // set camera position     

      // Projection gives us the depth and field of view

      program_state.projection_transform = Mat4.perspective(
         Math.PI / 4, context.width / context.height, 1, 500);

      // **Light** stores the properties of one light in a scene.  Contains a coordinate and a
      // color (each are 4x1 Vectors) as well as one size scalar.
      // The coordinate is homogeneous, and so is either a point or a vector.  Use w=0 for a
      // vector (directional) light, and w=1 for a point light / spotlight.
      // For spotlights, a light also needs a "size" factor for how quickly the brightness
      // should attenuate (reduce) as distance from the spotlight increases.
      //constructor(position, color, size) {
      // Object.assign(this, {position, color, attenuation: 1 / size});
      //  const light_position = vec4(10, 10, 10, 1);
  
      const light_position = vec4(10,10,10, 0);  // directional light (1 would be spotlight)
      program_state.lights = [new Light(light_position, color(1, 1, 1, 1), 10000)];

      this.draw_food(context, program_state, t);

      let obstacle_list = this.game.school.obstacle_list;
      for(let f of obstacle_list) {
          // console.log("drawing obstacle " + f.name);
          this.draw_obstacle(context, program_state, f);
      }

      let fish_list =  this.game.move(this.food_list, dt);
      for(let f of fish_list) {
          // console.log("drawing fish " + f.name);
          this.draw_fish(context, program_state, f);
      }
   }
}


class Texture_Scroll_X extends Textured_Phong {
    // TODO:  Modify the shader below (right now it's just the same fragment shader as Textured_Phong) for requirement #6.
    fragment_glsl_code() {
        return this.shared_glsl_code() + `
            varying vec2 f_tex_coord;
            uniform sampler2D texture;
            uniform float animation_time;
            
            void main(){
                // Sample the texture image in the correct place:

                vec2 vv = f_tex_coord.xy;   // copy picture coordinate into variable
                //vv.x = vv.x + .5;   // move it over
                vv.y = vv.y + mod(animation_time,2.);
                
                vec4 tex_color = texture2D( texture, vv.xy);
                // vec4 tex_color = texture2D( texture, f_tex_coord);

                // to make picture scroll, sample a different location

                if( tex_color.w < .01 ) discard;

                gl_FragColor = tex_color;

               // The next part washes out the image
                                                                         // Compute an initial (ambient) color:
               // gl_FragColor = vec4( ( tex_color.xyz + shape_color.xyz ) * ambient, shape_color.w * tex_color.w ); 
                                                                         // Compute the final color with contributions from lights:
              //  gl_FragColor.xyz += phong_model_lights( normalize( N ), vertex_worldspace );
        } `;
    }
}


class Texture_Rotate extends Textured_Phong {
    // TODO:  Modify the shader below (right now it's just the same fragment shader as Textured_Phong) for requirement #7.
    fragment_glsl_code() {
        return this.shared_glsl_code() + `
            varying vec2 f_tex_coord;
            uniform sampler2D texture;
            uniform float animation_time;
            void main(){
                // Sample the texture image in the correct place:
                vec4 tex_color = texture2D( texture, f_tex_coord );

                vec2 vv = f_tex_coord.xy;   // copy picture coordinate into variable

                vv.x -=.5;
                vv.y-=.5;

                float r = sqrt( vv.x * vv.x + vv.y*vv.y);
                vec2 nuv;
                float theta;
                if(vv.x==0.)theta = 3.14159/2.;
                else theta=atan(vv.y/vv.x);

                if(vv.x < 0.) theta = theta + 3.14159265;

                theta = theta + mod(animation_time,99.);

                nuv.x = r * cos(theta)+.5;
                nuv.y = r * sin(theta)+.5;






                vec4 tex_colorx = texture2D( texture, nuv.xy);
                gl_FragColor = tex_colorx;
          //      gl_FragColor.xyz *= phong_model_lights( normalize( N ), vertex_worldspace );


        //        return;

        //        if( tex_color.w < .01 ) discardr.xyz + shape_color.xyz ) * ambient, shape_color.w * tex_color.w ); 
                                               ;
                                                                         // Compute an initial (ambient) color:
        //        gl_FragColor = vec4( ( tex_colo                          // Compute the final color with contributions from lights:
        //        gl_FragColor.xyz += phong_model_lights( normalize( N ), vertex_worldspace );
        } `;
    }
}
