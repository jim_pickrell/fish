# README #

This is the archive for the UCLA CS275 project  on Fish Schooling and Shoaling, Spring 21

### What is this repository for? ###

* This project is based on the Reyholds article on schooling and shoaling.

### How do I get set up? ###

* Download and run the code.  This is Javascript, so there is no need to compile.
* This project uses the UCLA Tiny Graphics WebGL interface.
* You will need some kind of web server.  Apache works, or you can use the Python web server that is included here.

### Who do I talk to? ###

* For questions contact Jim Pickrell jimp@ucla.edu

* git test
